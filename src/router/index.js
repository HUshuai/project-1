import Vue from 'vue';
import VueRouter from 'vue-router';
import Map from './modules/authority';
import User from './modules/User';
import Text from './modules/text';
import classSes from './modules/ClassSes';
import Await from './modules/await'
Vue.use(VueRouter);

const routes = [
        {
            path:'/user/login',
            component:()=> import('@/views/login/index.vue')
        },
        {
            path:'/home',
            component:()=> import('@/views/home/index.vue'),
            children:[
                ...Map,
                ...User,
                ...Text,
                ...classSes,
                ...Await
            ]
        },
        {
            path:'/',
            redirect:'/user/login'
        }
    ]

const router = new VueRouter({mode: 'hash', base: process.env.BASE_URL,routes, });

export default router;
