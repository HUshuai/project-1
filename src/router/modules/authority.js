const map = [
  //添加试题
    {
      path:'/home/insertQuestionsType',
      component:() => import('@/views/home/authority/questions.vue')
    },
  //查看试题
    {
      path:'/home/questions/condition',
      component:() => import('@/views/home/authority/lock')
    },
  //试题分类
    {
      path:'/home/getQuestionsType',
      component:() => import('@/views/home/authority/insertQuestionsType')
    },
]

export default map;